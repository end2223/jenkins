#FROM node:14-stretch-slim as build
#WORKDIR /app
#COPY . /app
#RUN npm install && npm run build

#FROM nginx:latest

#WORKDIR /app
#COPY --from=build /app/dist/Jenkins /usr/share/nginx/html
#COPY ./nginx.conf /etc/nginx/nginx.conf

#RUN apt-get update && apt-get upgrade -y

#RUN chown -R nginx:nginx /app && chmod 755 /app && \
#    chown -R nginx:nginx /var/log/nginx && \
#    chown -R nginx:nginx /var/cache/nginx && \
#    chown -R nginx:nginx /etc/nginx/conf.d && \
#    chown nginx:nginx /etc/nginx/nginx.conf

#RUN touch /var/run/nginx.pid && \
#        chown -R nginx:nginx /var/run/nginx.pid

#EXPOSE 80

#USER nginx

# HEALTHCHECK --interval=30s --timeout=30s --start-period=20s --retries=3 CMD [ "" ]


FROM centos:centos7.9.2009
